---
title: Fix Color Emojis in Chrome on Linux
date: 2020-01-25
pubDate: 2020-01-25
description: Instructions for setting up color emojis on Linux... since that doesn't seem to be a priority in the "year of the Linux desktop".
keywords:
  - linux
  - config
layout: ../../layouts/Post.astro
---

This issue has been bothering me for quite some time now. The color emojis never work out of the box on any DE. At least when it comes to Chrome. Most distros now include Googles Noto Color Emoji font by default and it works on all native applications. Seems like Chrome doesn't pick it up for some reason. I'll go over a solution that fixed this situation for me and it might be the same for you.

This acually isn't originally my solution. You can find some posts [here](https://blog.livthomas.net/adding-color-emojis-to-chrome-on-fedora/) and [here](https://www.omgubuntu.co.uk/2016/08/enable-color-emoji-linux-google-chrome-noto) that offer the same solution.

This fairly simple fix has done it for me on Ubuntu (19.10) and Fedora (31), both runing Gnome (3.34). All you need to do is check if the color emoji package is installed on your system and add a short font config file in your local config directory.

|Distro|Package|
|-|-|
|Ubuntu|fonts-noto-color-emoji|
|Fedora|google-noto-emoji-color-fonts|

Now you'll just need to create a file in your users config directory.

💾️ `~/.config/fontconfig/fonts.conf`

Paste the following XML into the config file and save it.


```xml
<?xml version="1.0" encoding="UTF-8"?> 
<!DOCTYPE fontconfig SYSTEM "fonts.dtd"> 
<fontconfig> 
  <alias> 
    <family>serif</family> 
    <prefer> 
      <family>Noto Color Emoji</family> 
    </prefer> 
  </alias> 
  <alias>
    <family>sans-serif</family>
    <prefer>
      <family>Noto Color Emoji</family>
    </prefer>
  </alias>
  <alias>
    <family>monospace</family>
    <prefer>
      <family>Noto Color Emoji</family>
    </prefer>
  </alias>
</fontconfig>
```

Finaly, run the command for updating the fonts config.

```bash
$ fc-cache -f
```

Thats it! Just restart Chrome and you're good to go.
