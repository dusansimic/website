---
title: DrawingFX libjpeg fix on Linux
date: 2022-10-17
pubDate: 2022-10-17
description: Guide how to fix the issue with DrawingFX needing a newer version of libjpeg library on some Linux distros
keywords:
  - linux
  - university
layout: ../../layouts/Post.astro
---

DrawingFX is at the time of writing a library used on computer graphics courses at Faculty of Sciences, University of Novi Sad. The library is made by the course teacher and is a great tool for introducing students to computer graphics.

The only problem is that it depends on JavaFX and some functionalities require a higher version of the libjpeg library than the one that is available on many Linux distributions (currently the only validated one that has the required version is Ubuntu so the problem doesn't occur there).

The fix is fairly easy and any user who was able to install Linux should be able to resolve the issue. If any problems arise, there are teaching assistants and professors who could help you out.

## System provided libjpeg

Most Linux distros provide libjpeg 6.2 with a compatibility layer for software that requires a higher version. This however doesn't do the trick in case of JavaFX (and in turn DrawingFX) which throws an error about having an Image object with non-positive dimensions. This happens when the image is not loaded corretly because of the older version of libjpeg. The image doesn't fail to load, it just loads incorretly. This is due to the compatibility layer in the libjpeg version distributed on most distributions.

```
Exception in thread "JavaFX Application Thread" java.lang.IllegalArgumentException: Image dimensions must be positive (w,h > 0)
```

## Adding the correct version of libjpeg

Most major distros should have the newer version packaged in third party repositories so if you're using a major distro (Fedora, RHEL, Arch...) you're safe.

### Fedora

You'll need to add [aflyhorse/libjpeg](https://copr.fedorainfracloud.org/coprs/aflyhorse/libjpeg/) COPR repo to your Fedora installation. At the time of writing you can do that by running the following command as root.

```sh
dnf copr enable aflyhorse/libjpeg
```

That will add the required repo to the repo list. After that you'll need to install the `libjpeg9` package.

```sh
dnf install libjpeg9
```

Take a note what is the path to the shared library file for the installed version. It's probably going to be `/usr/lib64/libjpeg.so.9` but you should double check that before continuing. You can get those paths by running `rpm -ql libjpeg9 | grep so` (list the paths of all files that are provided by libjpeg9 package and grep only those lines that contain characters 'so' which is an extension for shared library files).

From then on, the configuration is the same on every distro.

### Arch

The required package is available in the [AUR](https://aur.archlinux.org/) so you can either build and install the package yourself or use an [AUR helper](https://wiki.archlinux.org/title/AUR_helpers) to do it for you. The package you'll need is `libjpeg9`. I expect that you can manage installing it if you're running Arch 😉.

Take a note what is the path to the shared library file for the installed version. It's probably going to be `/usr/lib/libjpeg.so.9` but you should double check that before continuing. You can get those paths by running `pacman -Ql libjpeg9 | grep so` (list the paths of all files that are provided by libjpeg9 package and grep only those lines that contain characters 'so' which is an extension for shared library files).

After that, the rest of configuration is the same on every distro.

## Configuring your development environment

The only thing that you need to do is to set the `LD_PRELOAD` variable to the path to shared library file for your specific distro. You can do that in any environment, including when you run all the commands by hand. The reason for doing that is so DrawingFX doesn't load the system provided libjpeg but the one we want it to load (the newer version). Without it, DrawingFX still load the old version if tho the new one is installed on the system.

Here are some common environments documented so it's easier for students to configre their setup.

### Eclipse

You should be able to set the required variable by going to Run -> Run configurations... and on the open window, select tab 'Environment'. There you can at the variable.

## Conclusion

This is an issue I've stumbled upon during my studies and one many other students did too. I've managed to track down the root of the issue and found a way to resolve it so I shared it at the time with other students. I however think that not everyone needs to go through the same debugging process as I did so I hope this post helps those poor souls that are running distros which didn't yet switch to a newer version of libjpeg.

If this solution hasn't worked for you, you've found another family of distros that has this issue but is not documented here or want to document another environement, you can reach out to me via email so I can patch this post.
