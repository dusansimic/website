---
title: Why I love RSS feeds
date: 2020-11-08
pubDate: 2020-11-08
keywords:
  - misc
layout: ../../layouts/Post.astro
---

I hate it when I have to use buggy apps that track your every tap or hard to use API just to read my notifications. It's mind boggling that after all these years that internet exists, we still have to hack around restrictions and walls that have been put up by giant corporations to entrap you in their playground.

I'm not a freedom fanatic or anything like uncle Stallman but I prefer to use apps that I pick to access media. I don't like being forced to use some specific app or service to get my hands on essential information. There is no excuse to force users to use closed and proprietary services except wanting to control and track their usage.

People say it's "easier" to use those apps. No it's not. Is it easier to use multiple messaging apps (Messenger, Instagram, Viber, WhatsApp, Slack and Discord) or to have one app that can connect to all of them at once? It's as simple as that.

And basically that's why I prefer using RSS feed over singing in to the service to see my notifications. I like the simplicity of the RSS reader and ability to read all of my notifications in one place. I've started a [project](https://github.com/dusansimic/feedgen) to scrape information and notifications from services that don't offer RSS feeds and create an RSS feed for them. I know there's [RSS-Bridge](https://github.com/RSS-Bridge/rss-bridge) but TBH, I don't like the feed format that it output's and I like to get my hands dirty and learn new technologies while solving problems.

For those interested in what I use to read RSS feeds, it's a Gnome app called [Feeds](https://gabmus.gitlab.io/gnome-feeds/). There are lots of other RSS readers but I like this one so far. I might explore web based readers like [Miniflux](https://miniflux.app/) or some more advnaced desktop readers but this seems fine for now.
