---
title: Messing Around With Golang
date: 2020-11-06
pubDate: 2020-11-06
description: At the start of the pandemic I wanted to try out some new language to write backends in so it could run on 32bit machines. I've decided on Go.
keywords:
  - dev
  - golang
layout: ../../layouts/Post.astro
---

I've mostly written backend stuff in high school with some frontend and on rare ocasions a bit of embedded code (Arduino and whatnot). I fell in love with internet, web services and the way goes around the world relying on "the cloud ☁".

I started my journey working with a bit of Node.js. I remember it from the days since before 1.0 😱 but I've used it mostly in later versions. A classical beginning, Express and MongoDB for database. As the time passed I started to be more and more interested in the fullstack stuff and started to work my way through Vue. It was easy to understand it compared to React and Angular. At that point I was mediocre in fullstack technologies and I was just playing around with all of that stuff. It was interesting to me and I was a nerd. I mean, I was already rocking Linux as my main os 🙄.

I wanted to get real and work on some more ambitious projects, not just tiny "get to know the framework" projects. I've started working in TypeScript since all the cool kids were using it and it actually solved a lot of problems I had with JavaScript. It was cool and everything but then I also started messing arround with an old 32bit laptop that I had laying around. I've set it up as a server and I wanted to run backend for my projects on it. As some more perceptive of you probably already realized, there is no Node.js support for 32bit machines so that was a problem. I really wanted to run stuff on that machine since I love messing around with servers. There were two easy options, Python or something that can compile to 32bit architecture (I wasn't even gonna consider php, fuck that). I thought about C++ or Java but I was very lazy and didn't wanna spend a lot of time on learning all the caveats of those languages. Java could be a nice choice (or Kotlin) but I was very lazy and also liked the simplicity of JavaScript and TypeScript. Then I remembered Go.

And boy did I like it. I've heard about Go a very long time ago (like 5 years) but saw the syntax for the first time in my sophomore year of high school. At the time I didn't make much out of it but I really like it now. It's very clean, simple, easy to understand and the best practices are for a beginner maybe a little bit weird but when you think about them, they actually make a lot of sense. It's fast, hast a great community, easy to work with and it's very easy to learn. Error handling is a bit of a set back compared to TypeScript but I like it. As our penguin king Linus Torvalds says, "It does what you tell it to do and nothing more." He was however talking about computers in general and not about Golang but just minor details 😉. It's easy to know what the code does at every point since it's so simple. I know it's not a perfect tool for every job but for small to medium sized backends for personal projects, it's a neat little tool. I think even some bank called Monzo uses it for their services. It's very easy to write tests to since it has an integrated testing framework, alongside a package manager.

All in all, I like it. We're working in Java at the Uni right now so I might later check out some web frameworks over there and start deploying stuff for real in Docker/Podman. I still stick to JS/TS for frontends, lately I've been working with React mostly and I like it, but for backends, Golang is a win 🚀.
