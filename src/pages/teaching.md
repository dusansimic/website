---
title: Teaching - Dušan Simić
layout: ../layouts/Empty.astro
---

## Teaching

I'm working as an teaching associate (TA) for the Chair of Computer Science at
the Faculty of Sciences, University of Novi Sad, Serbia where I'm assisting on
the following courses:

- Data Structures and Algorithms 2 (IT202)
- System Programming (IT302)
- Computer Organisation (IT616)

For any questions related to those subjects or any other teaching related
questions, please contact me at [dusan.simic@dmi.uns.ac.rs](mailto:dusan.simic@dmi.uns.ac.rs)
