import {Frontmatter} from '../layouts/Post.astro'

// Implementation of the spaceship operator from PHP
export const spaceship = (a: { frontmatter: Frontmatter }, b: { frontmatter: Frontmatter }) => {
	if (a.frontmatter.date < b.frontmatter.date) return -1;
	else if (a.frontmatter.date > b.frontmatter.date) return 1;
	else return 0;
}

// Creates an inverse function from the passed one (f -> -f)
export const inverse = (f: (...args: any[]) => number) => (...args: any[]) => -f(...args)