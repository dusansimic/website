/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{astro,html,js,jsx,md,svelte,ts,tsx,vue}'],
	theme: {
		extend: {
			fontFamily: {
				'sans': ['system-ui', 'Ubuntu', '-apple-system', 'BlinkMacSystemFont', 'Segoe UI'],
			},
			width: {
				'60ch': '60ch',
			},
		},
	},
	plugins: [
		require('@tailwindcss/typography'),
	],
}
