import { defineConfig } from 'astro/config';

import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  integrations: [tailwind()],
  site: 'https://dusansimic.me',
  // These settings are here becauses of GitLab pages requirement
  outDir: 'public',
  publicDir: 'static',
});
